Pod::Spec.new do |s|
  s.name             = 'pguzman2017'
  s.version          = '0.1.0'
  s.summary          = 'ARC and GCD Compatible Reachability Class for iOS and OS X.'
  s.description      = 'Lorem ipsum dolor sit amet, causae alienum at sit, quo an accusam nominati. Ex sea iriure percipitur. Duo eu populo tritani viderer. Ut eos erant eripuit cotidieque, qui falli mucius noster ad, eos ei accusata gloriatur. His at tale audiam vivendum, duo suas possit no.'

  s.homepage         = 'https://github.com/pguzman42'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'pguzman42' => 'pguzmanosorio@gmail.com' }
  s.source           = { :git => 'https://github.com/pguzman42/podpod.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  s.ios.deployment_target = '8.0'
  s.source_files = 'pguzman2017/Classes/**/*'
  s.frameworks = 'UIKit', 'MapKit', 'CoreData'
end

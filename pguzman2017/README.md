# pguzman2017

[![CI Status](http://img.shields.io/travis/pguzman42/pguzman2017.svg?style=flat)](https://travis-ci.org/pguzman42/pguzman2017)
[![Version](https://img.shields.io/cocoapods/v/pguzman2017.svg?style=flat)](http://cocoapods.org/pods/pguzman2017)
[![License](https://img.shields.io/cocoapods/l/pguzman2017.svg?style=flat)](http://cocoapods.org/pods/pguzman2017)
[![Platform](https://img.shields.io/cocoapods/p/pguzman2017.svg?style=flat)](http://cocoapods.org/pods/pguzman2017)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

pguzman2017 is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'pguzman2017'
```

## Author

pguzman42, pguzmanosorio@gmail.com

## License

pguzman2017 is available under the MIT license. See the LICENSE file for more info.

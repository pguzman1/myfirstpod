//
//  ArticleManager.swift
//  Pods
//
//  Created by Patricio GUZMAN on 10/12/17.
//
//

import Foundation
import CoreData

@available(iOS 10.0, *)
public class ArticleManager: NSObject {
    
    var managedObjectContext: NSManagedObjectContext
    var controller: NSFetchedResultsController<Article>?
    
    
    
    public init(context: NSManagedObjectContext) {
        self.managedObjectContext = context
        
        let podBundle = Bundle(for: Article.self)
        let url = podBundle.url(forResource: "article", withExtension: "momd")
        guard let mom = NSManagedObjectModel(contentsOf: url!) else {
            fatalError("Error initializing mom from: \(url!)")
        }
        
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
        
        let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
        queue.async {
            guard let docURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last else {
                fatalError("Unable to resolve document directory")
            }
            let storeURL = docURL.appendingPathComponent("pguzman2017.sqlite")
            do {
                try psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
            } catch {
                fatalError("Error migrating store: \(error)")
            }
        }
    }
    
    public func attemptFetch() {
        let fetchRequest: NSFetchRequest<Article> = Article.fetchRequest()
        let dateSort = NSSortDescriptor(key: "creation_date", ascending: false)
        fetchRequest.sortDescriptors = [dateSort]
        
        controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        do {
            try controller!.performFetch()
        }
        catch {
            let error = error as NSError
            print("\(error)")
        }
    }
    
    public func newArticle() -> Article {
        let newArticle = Article(context: managedObjectContext)
        return newArticle
    }
    
    public func getAllArticles() -> [Article]? {
        let fetchRequest: NSFetchRequest<Article> = Article.fetchRequest()
        let dateSort = NSSortDescriptor(key: "creation_date", ascending: false)
        fetchRequest.sortDescriptors = [dateSort]
        
        controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        do {
            try controller!.performFetch()
        }
        catch {
            let error = error as NSError
            print("\(error)")
        }
        return controller?.fetchedObjects
    }
    
    public func getArticles(withLang lang: String) -> [Article]? {
        let fetchRequest: NSFetchRequest<Article> = Article.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "lang == %@", lang)
        let dateSort = NSSortDescriptor(key: "creation_date", ascending: false)
        fetchRequest.sortDescriptors = [dateSort]
        
        controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        do {
            try controller!.performFetch()
        }
        catch {
            let error = error as NSError
            print("\(error)")
        }
        return controller?.fetchedObjects
    }
    
    public func getArticles(containString str: String) -> [Article]? {
        let fetchRequest: NSFetchRequest<Article> = Article.fetchRequest()
        let firstPredicate = NSPredicate(format: "content CONTAINS %@", str)
        let secondePredicate = NSPredicate(format: "title CONTAINS %@", str)

        let predicate = NSCompoundPredicate(type: .or, subpredicates: [firstPredicate, secondePredicate])
        let dateSort = NSSortDescriptor(key: "creation_date", ascending: false)
        fetchRequest.sortDescriptors = [dateSort]
        fetchRequest.predicate = predicate 
        controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        do {
            try controller!.performFetch()
        }
        catch {
            let error = error as NSError
            print("\(error)")
        }
        return controller?.fetchedObjects
    }
    
    public func removeArticle(article: Article) {
        self.managedObjectContext.delete(article)
    }
    
    public func save() {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            }
            catch {
                let error = error as NSError
                print("\(error)")
            }
        }
    }
    
}
//UIApplicationDelegate,
@available(iOS 10.0, *)
extension ArticleManager: NSFetchedResultsControllerDelegate {
    
}

var applicationDocumentsDirectory: URL = {
    
    let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return urls[urls.count-1]
}()

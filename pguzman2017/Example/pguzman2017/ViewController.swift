//
//  ViewController.swift
//  pguzman2017
//
//  Created by pguzman42 on 10/12/2017.
//  Copyright (c) 2017 pguzman42. All rights reserved.
//

import UIKit
import pguzman2017
import CoreData


let ad = UIApplication.shared.delegate as! AppDelegate
let context = ad.persistentContainer.viewContext


class ViewController: UIViewController {

    let articleManager = ArticleManager(context: context)
    override func viewDidLoad() {
        super.viewDidLoad()
        createAnArticle("article 1", "this is my first article", "fr")
        createAnArticle("article 2", "this is another article", "fr")
        createAnArticle("article 3", "this is another article", "fr")
        createAnArticle("article 4", "this is my sadfdasffirst article", "en")
        createAnArticle("article 5", "this is anotasdfafdsher article", "en")
        createAnArticle("article 6", "this is anoafsdadsfafdfdther article", "en")
        articleManager.save()

        var articles = articleManager.getAllArticles()
        for article in articles! {
            print(article.title!)
        }
        print("-----")
        articles = articleManager.getArticles(withLang: "en")
        for article in articles! {
            print(article.title!)
        }
        print("-----")
        articles = articleManager.getArticles(withLang: "fr")
        for article in articles! {
            print(article.title!)
        }
        print("-----")
        articles = articleManager.getArticles(containString: "my")
        for article in articles! {
            print(article.title!)
        }
    }
    
    func createAnArticle(_ title: String,_ content: String,_ lang: String) {
        let article2 = articleManager.newArticle()
        article2.content = content
        article2.creation_date = NSDate()
        article2.modification_date = NSDate()
        article2.lang = lang
        article2.title = title
    }


}

